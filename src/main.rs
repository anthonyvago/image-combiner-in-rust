mod args;
use args::Args;

use image::{
    imageops::FilterType::Triangle, io::Reader, DynamicImage, GenericImageView, ImageFormat, ImageError
};
use std::convert::TryInto;

#[derive(Debug)]
enum ImageDataErrors {
    DifferentImageFormats,
    BufferTooSmall,
    UnableToReadImageFromPath(std::io::Error),
    UnableToFormatImage(String),
    UnableToDecodeImage(ImageError),
    UnableToSaveImage(ImageError),
}

struct FloatingImage {
    w: u32,
    h: u32,
    data: Vec<u8>,
    name: String,
}

impl FloatingImage {
    fn new(w: u32, h: u32, name: String) -> Self {
        let buff_cap = h * w * 4;
        let buff = Vec::with_capacity(buff_cap.try_into().unwrap());
        FloatingImage {
            w,
            h,
            data: buff,
            name,
        }
    }

    fn set_data(&mut self, data: Vec<u8>) -> Result<(), ImageDataErrors> {
        if data.len() > self.data.capacity() {
            return Err(ImageDataErrors::BufferTooSmall);
        }
        self.data = data;
        Ok(())
    }
}

fn main() -> Result<(), ImageDataErrors> {
    let args = Args::new();
    let (img_1, img_format_1) = find_image_from_path(args.img_1)?;
    let (img_2, img_format_2) = find_image_from_path(args.img_2)?;

    if img_format_1 != img_format_2 {
        return Err(ImageDataErrors::DifferentImageFormats);
    }

    let (_img_1, _img_2) = standardise_size(img_1, img_2);
    let mut output = FloatingImage::new(_img_1.width(), _img_1.height(), args.output);

    let combined_data = combine_images(_img_1, _img_2);
    output.set_data(combined_data)?;

    if let Err(e) = image::save_buffer_with_format(output.name, &output.data, output.w, output.h, image::ColorType::Rgba8, img_format_1) {
        Err(ImageDataErrors::UnableToSaveImage(e))
    } else {
        Ok(())
    }
}

fn find_image_from_path(path: String) -> Result<(DynamicImage, ImageFormat), ImageDataErrors> {
    match Reader::open(&path) {
        Ok(img_reader) => {
            if let Some(img_format) = img_reader.format() {
                match img_reader.decode() {
                    Ok(img) => Ok((img, img_format)),
                    Err(e) => Err(ImageDataErrors::UnableToDecodeImage(e))
                }
            } else {
                return Err(ImageDataErrors::UnableToFormatImage(path));
            }
        },
        Err(e) => Err(ImageDataErrors::UnableToReadImageFromPath(e))
    }
}

fn get_smallest_dimension(dim_1: (u32, u32), dim_2: (u32, u32)) -> (u32, u32) {
    let pix_1 = dim_1.0 * dim_1.1;
    let pix_2 = dim_2.0 * dim_2.1;
    return if pix_1 < pix_2 { dim_1 } else { dim_2 };
}

fn standardise_size(img_1: DynamicImage, img_2: DynamicImage) -> (DynamicImage, DynamicImage) {
    let (w, h) = get_smallest_dimension(img_1.dimensions(), img_2.dimensions());

    println!("w: {}, h: {}", w, h);

    if img_2.dimensions() == (w, h) {
        // if image 2 is the smallest of the two:
        return (img_1.resize_exact(w, h, Triangle), img_2);
    } else {
        return (img_1, img_2.resize_exact(w, h, Triangle));
    }
}

fn combine_images(img1: DynamicImage, img2: DynamicImage) -> Vec<u8> {
    let vec1 = img1.to_rgba8().into_vec();
    let vec2 = img2.to_rgba8().into_vec();
    alternate_pixels(vec1, vec2)
}

fn alternate_pixels(vec1: Vec<u8>, vec2: Vec<u8>) -> Vec<u8> {
    let mut combined_data = vec![0u8; vec1.len()];

    let mut i = 0;
    while i < vec1.len() {
        if i % 8 == 0 {
            combined_data.splice(i..=i + 3, set_rgba(&vec1, i, i + 3));
        } else {
            combined_data.splice(i..=i + 3, set_rgba(&vec2, i, i + 3));
        }
        i += 4;
    }
    return combined_data;
}

fn set_rgba(vec: &Vec<u8>, start: usize, end: usize) -> Vec<u8> {
    let mut rgba = Vec::new();
    for i in start..=end {
        let val: u8 = match vec.get(i) {
            Some(d) => *d,
            None => panic!("Index out of bounds!"),
        };
        rgba.push(val);
    }
    return rgba;
}
